<section id="header" data-module="header">
    <div class="main_header_content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item {{ url_path() === null ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('/') }}">Upload File</a>
                    </li>
                    <li class="nav-item {{ url_path() === '/productsList' ? 'active' : ''}}">
                        <a class="nav-link" href="{{ url('/productsList') }}">Products List</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</section>
