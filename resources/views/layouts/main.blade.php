<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" id="meta_csrf" content="{{ csrf_token() }}">
    <title>Local Express Test</title>
    <link rel="stylesheet" href="{{ asset('localExpress/dist/main.css') }}">
    @yield('data')
</head>
<body>
@include('layouts._header')
@yield('content')
<div data-module="mobile-nav-background"></div>
<script src="{{ asset('localExpress/dist/main.js') }}"></script>
</body>
</html>
