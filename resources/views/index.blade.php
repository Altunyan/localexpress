@extends('layouts.main')

@section('content')
    <div data-module="upload-files">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center"><h2>Multiple File Uploading</h2></div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 upload-file-block">
                        <div class="form-group">
                            <label for="select-store">Select Store</label>
                            <select name="store" id="select-store">
                                @foreach($stores as $store)
                                    <option value="{{$store->id}}">{{$store->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="Product Name">Import Files (can attach more than one):</label>
                        <br/>

                        <input type="file" id="files" class="form-control" name="files[]" multiple required/>
                        <br/><br/>
                        <div class="alert alert-danger upload-file-alert hidden" role="alert">
                            Max uploading size 5MB
                        </div>
                        <button id="upload-file" class="btn btn-primary" type="button">
                            <span class="spinner-border spinner-border-sm hidden" role="status" aria-hidden="true"></span>
                            Upload
                        </button>

                </div>
            </div>
        </div>
    </div>
@endsection