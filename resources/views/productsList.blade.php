@extends('layouts.main')

@section('content')
<div data-module="products-tables" class="tables">
    <div class="files">
        <h3>Files</h3>
        <table class="files-table">
            <tr>
                <th>File Name</th>
                <th>Store</th>
                <th>Status</th>
                <th>Size</th>
                <th>Problem Columns Count</th>
                <th>Import Starting Time</th>
                <th>Created At</th>
            </tr>
            @foreach($files as $file)
                <tr>
                    <td>{{$file->file_name}}</td>
                    <td>{{$file->store_name}}</td>
                    <td>{{$file->status}}</td>
                    <td>{{$file->size}}KB</td>
                    <td>{{$file->error_columns_count ? $file->error_columns_count : 'N/A'}}</td>
                    <td>{{$file->import_starting_time ? $file->import_starting_time : 'N/A'}}</td>
                    <td>{{$file->created_at}}</td>
                </tr>
            @endforeach
        </table>
        {{$files->links()}}
    </div>
    <div class="products">
        <h3>Products</h3>
        <table class="files-table">
            <tr>
                <th>UPC</th>
                <th>Title</th>
                <th>Price</th>
                <th>File Name</th>
                <th>Store</th>
                <th>Created At</th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->upc}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->file_name}}</td>
                    <td>{{$product->store_name}}</td>
                    <td>{{$product->created_at}}</td>
                </tr>
            @endforeach
        </table>
        {{$products->links()}}
    </div>
</div>
@endsection

