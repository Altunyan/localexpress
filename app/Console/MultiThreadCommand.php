<?php

namespace App\Console;


class MultiThreadCommand
{
    /**
     * Commands to run
     *
     * @var array
     */
    private $commands = [];

    /**
     * Output streams of processes
     *
     * @var array
     */
    private $streams = [];

    /**
     * Processes
     *
     * @var array
     */
    private $processes = [];

    /**
     * proc_open returned pipes array
     *
     * @var array
     */
    private $pipes = [];

    /**
     * MultiThreadCommand constructor.
     *
     * @param $commands
     */
    public function __construct($commands)
    {
        $this->commands = $commands;
    }

    /**
     * Run commands
     *
     * @return array
     */
    public function run()
    {
        $processReturnValues = [];

        //Run processes
        foreach ($this->commands as $key => $command) {
            $descriptors = [
                0 => ['pipe', 'r'], // STDIN
                1 => ['pipe', 'w']  // STDOUT
            ];
            $this->processes[] = proc_open($command, $descriptors, $pipes);
            $this->streams[] = $pipes[1];
            $this->pipes[] = $pipes;
            usleep(500000); //Sleep 0.5 sec between processes
        }

        $w = null;
        $e = null;
        //Get streams' outputs
        while (count($this->streams)) {
            $outs = $this->streams;
            stream_select($outs, $w, $e, 0);
            foreach ($outs as $out) {
                $key = array_search($out, $this->streams);
                echo fgets($out);

                if (feof($out)) {
                    fclose($this->pipes[$key][0]);
                    fclose($this->pipes[$key][1]);
                    $processReturnValues[] = proc_close($this->processes[$key]);
                    unset($this->streams[$key]);
                }
            }
        }

        return $processReturnValues;
    }
}