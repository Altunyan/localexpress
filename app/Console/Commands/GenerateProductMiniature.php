<?php

namespace App\Console\Commands;

use App\Exceptions\NoSuchFileOrDirectoryException;
use App\Helpers\ImageHelper;
use App\Models\MySQL\MiniatureProduct;
use App\Models\MySQL\MiniatureStoreProduct;
use Illuminate\Console\Command;

class GenerateProductMiniature extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateProductMiniature {size}
    								{--watermarked=false : watermarked}
    								{--catalogOnly=true : catalogOnly}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$size = $this->argument('size');
		$watermarked = $this->option('watermarked') === 'true' ? true : false;
		$catalogOnly = $this->option('catalogOnly') === 'true' ? true : false;
		$successfullyGenerated = 0;
		$notGenerated = 0;

		if ($catalogOnly) {
			$products = MiniatureStoreProduct::leftJoin('miniature_products', 'miniature_products.id', '=', 'miniature_store_products.product_id')
				->where('miniature_products.is_deleted', 0)
				->select('miniature_products.image')
				->get();
		} else {
			$products = MiniatureProduct::where('miniature_products.is_deleted', 0)
				->select('miniature_products.image')
				->get();
		}

		$sizes = explode('x', $size);

		if (count($sizes) == 1) {
			$sizes[1] = $sizes[0];
		}

		foreach ($products as $product) {
			try {
				$src = public_path() . '/productImages/' . $product->image;

				if ($watermarked) {
					ImageHelper::generateMiniature($src, $sizes);
				} else {
					ImageHelper::generateWatermarkedMiniature($src, $sizes);
				}
				$successfullyGenerated++;
			} catch (NoSuchFileOrDirectoryException $e) {
				$notGenerated++;
			} catch (\Exception $e) {
				$notGenerated++;
			}
		}

		print 'Successfully Generated - ' . $successfullyGenerated . PHP_EOL;
		print 'Not Generated - ' . $notGenerated . PHP_EOL;
    }
}
