<?php

namespace App\Console\Commands;

use App\Console\MultiThreadCommand;
use App\Models\MySQL\ImportFiles;
use App\Models\MySQL\Store;
use App\Models\MySQL\StoreProduct;
use App\Models\MySQL\ThreadHash;
use Illuminate\Console\Command;

class ImportFileData extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'importFileData 
    								{--threadHash= : threadHash}
    								{--threadHashesId= : threadHashesId}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		print 'started';

		$threadHash = $this->option('threadHash'); //one thread hash
		$threadHashesId = $this->option('threadHashesId'); //for multiply thread using

		$threadHashes = ThreadHash::find($threadHashesId);
		$threadHashes = $threadHashes ? explode(',', $threadHashes->hashes) : null;

		if ($threadHashes && count($threadHashes) > 1) {
			// commands for multi thread using(max count of array is a count threads of processor)
			$commands = [];
			$pathToArtisan = base_path('artisan');

			foreach ($threadHashes as $threadHash) {
				$commands[] = "php $pathToArtisan importFileData --threadHash=$threadHash &";
			}

			// custom class for use multi threads running
			$mc = new MultiThreadCommand($commands);
			$mc->run();
		} else {
			// when not exist one threads hash and in multiply thread running hashes have 1 hash
			!$threadHash && count($threadHashes) == 1 ? $threadHash = $threadHashes[0] : null;

			$importFiles = ImportFiles::where('thread_hash', $threadHash)
				->where('status', 'new')
				->get();

			foreach ($importFiles as $importFile) {
				$importFile->import_starting_time = date('Y-m-d H:i:s');
				$importFile->status = 'processing';
				$importFile->save();

				$fileName = $importFile->file_name;

				$data = $this->getFileContent($fileName);

				$this->addFileDataToStoreProduct($importFile, $data);
			}
		}

		print 'Done!';
	}

	/**
	 * getting file data for save in db
	 *
	 * @param $fileName
	 * @return array
	 */
	public function getFileContent($fileName)
	{
		$header = null;
		$data = [];

		if (($handle = fopen(public_path('uploads') . '/' . $fileName, 'r')) !== false) {
			while (($row = fgetcsv($handle, 0, ';')) !== false) {
				if (!$header) {
					$header = $row;
				} else {
					$data[] = array_combine($header, $row);
				}

				if (count($data) > 10000) {
					break;
				}
			}
			fclose($handle);
		}

		return $data;
	}

	/**
	 * add(update) file content data to(in) store
	 *
	 * @param $importFile
	 * @param $data
	 */
	public function addFileDataToStoreProduct($importFile, $data)
	{
		$errorColumnsCount = 0;

		foreach ($data as $key => $datum) {
			$datum += [
				'store_id' => $importFile->store_id,
				'file_id' => $importFile->id
			];

			if ($datum['upc']) {
				StoreProduct::updateOrCreate(['upc' => $datum['upc']], $datum);
			} else {
				$errorColumnsCount += 1;
				$datum += ['is_error' => 1];

				StoreProduct::create($datum);
			}

			if ($key == 2000) {
				break;
			}
		}

		$importFile->error_columns_count = $errorColumnsCount;
		$importFile->status = 'done';
		$importFile->save();
	}
}
