<?php

namespace App\Helpers;



use App\Exceptions\NoSuchFileOrDirectoryException;
use Intervention\Image\Facades\Image;

class ImageHelper
{
	public static function imageCreate($fileName)
	{
		$ext = pathinfo($fileName, PATHINFO_EXTENSION);

		if ($ext === 'jpg') $ext = 'jpeg';

		$functionName = "imagecreatefrom$ext";

		$image = $functionName($fileName);

		return $image;
	}

	public static function resize($width, $height, $fileName)
	{
		$originalFileInfo = getimagesize($fileName);
		$originalWidth = $originalFileInfo[0];
		$originalHeight = $originalFileInfo[1];

		$originalImage = static::imageCreate($fileName);

		$thumbImage = imagecreatetruecolor($width, $height);
		imagecopyresampled($thumbImage, $originalImage,
			0, 0,
			0, 0,
			$width, $height,
			$originalWidth, $originalHeight);

		return $thumbImage;
	}

	/**
	 * @param string $src
	 * @param array $sizes
	 * @throws NoSuchFileOrDirectoryException
	 */
	public static function generateMiniature($src, $sizes)
	{
		$isExists = file_exists($src);

		if (!$isExists) {
			throw new NoSuchFileOrDirectoryException('No Such File Or Directory');
		}

		$width = $sizes[0];
		$height = $sizes[1];

		$file = static::resize($width, $height, $src);
//		header('Content-type: image/png');
//		return imagepng($file);
	}

	/**
	 * @param string $src
	 * @param array $sizes
	 * @throws NoSuchFileOrDirectoryException
	 */
	public static function generateWatermarkedMiniature($src, $sizes)
	{
		$isExists = file_exists($src);

		if (!$isExists) {
			throw new NoSuchFileOrDirectoryException('No Such File Or Directory');
		}

		$width = $sizes[0];
		$height = $sizes[1];

		$watermark = static::resize($width, $height, public_path() . '/productImages/watermark.png');
		$image = static::resize($width, $height, $src);

		$marge_right = 0;
		$marge_bottom = 0;
		$sx = imagesx($watermark);
		$sy = imagesy($watermark);

		imagecopymerge($image, $watermark,
			imagesx($image) - $sx - $marge_right, imagesy($image) - $sy - $marge_bottom,
			0, 0,
			imagesx($watermark), imagesy($watermark), 50
		);

//		header('Content-type: image/png');
//		return imagepng($image);
	}
}