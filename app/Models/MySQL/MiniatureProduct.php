<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class MiniatureProduct extends Model
{
	protected $fillable = [
		'image',
		'is_deleted'
	];
}