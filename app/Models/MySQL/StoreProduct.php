<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class StoreProduct extends Model
{
    protected $fillable = [
    	'store_id',
    	'file_id',
    	'upc',
    	'title',
    	'price',
    	'is_error',
	];
}
