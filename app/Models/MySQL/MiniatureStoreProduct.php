<?php
namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class MiniatureStoreProduct extends Model
{
	protected $fillable = [
		'product_id',
		'product_image'
	];
}