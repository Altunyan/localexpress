<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class ImportFiles extends Model
{
    protected $fillable = [
		'store_id',
		'real_name',
		'file_name',
		'status',
		'size',
		'error_columns_count',
		'import_starting_time',
		'thread_hash',
	];
}
