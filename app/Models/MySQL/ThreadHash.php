<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class ThreadHash extends Model
{
    protected $fillable = ['hashes'];
}
