<?php

namespace App\Http\Controllers;

use App\Console\MultiThreadCommand;
use App\Helpers\ImageHelper;
use App\Jobs\ImportFileData;
use App\Models\MySQL\ImportFiles;
use App\Models\MySQL\Store;
use App\Models\MySQL\StoreProduct;
use App\Models\MySQL\ThreadHash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;

class StoreController extends Controller
{
	public function index()
	{
		$stores = Store::all();
		
		return view('index', ['stores' => $stores]);
	}

	/**
	 * upload files
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function uploadFiles(Request $request)
	{
		try {
			// file importing threads using count
			//(for optimal working set max value is processor threads max count)
			$threads = 2;

			if ($request->hasFile('files')) {
				$allowedfileExtension = ['csv', 'xlsx'];
				$files = $request->file('files');
				$storeId = $request->store;
				$fileMaxSize = 5 * 1024 * 1024; //5MB

				$threadHashes = [];
				$currentUsingThreadIndex = 1;

				foreach ($files as $index => $file) {
					$size = $file->getSize();

					if ($size > $fileMaxSize) continue;

					$milliseconds = round(microtime(true) * 1000);

					if ($currentUsingThreadIndex > $threads) {
						$currentUsingThreadIndex = 1;
					}

					// set hash for import data by file creation date
					if (!isset($threadHashes[$currentUsingThreadIndex])) {
						$threadHashes[$currentUsingThreadIndex] = md5($milliseconds);
					}

					// add file to directory
					$realName = $file->getClientOriginalName();
					$size = round($size / 1024); //size by KB
					$extension = $file->getClientOriginalExtension();
					$check = in_array($extension, $allowedfileExtension);
					$fileName = md5($milliseconds . $realName) . ".$extension";

					$file->move(public_path('uploads'), $fileName);

					if ($check && $size ) {
						ImportFiles::create([
							'store_id' => $storeId,
							'real_name' => $realName,
							'file_name' => $fileName,
							'size' => $size,
							'thread_hash' => $threadHashes[$currentUsingThreadIndex]
						]);

						$currentUsingThreadIndex++;
					}
				}

				$threadHashes = implode(',', $threadHashes);

				$threadHash = ThreadHash::create(['hashes' => $threadHashes]);
				
				$response = [
					'message' => 'Success',
					'status' => 200,
					'hashesId' => $threadHash->id,
					'redirectUrl' => 'productsList'
				];
			} else {
				$response = [
					'status' => Response::HTTP_BAD_REQUEST,
					'message' => 'Oops Something Went Wrong'
				];
			}
		} catch (\Exception $e) {
			$response = [
				'status' => Response::HTTP_BAD_REQUEST,
				'message' => 'Oops Something Went Wrong'
			];
		}

		return response()->json($response, $response['status']);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function productsList()
	{
		$files = ImportFiles::leftJoin('stores', 'import_files.store_id', '=', 'stores.id')
			->select('import_files.*', 'stores.name as store_name')
			->paginate(150)
			->setPageName('files');

		$products = StoreProduct::leftJoin('stores', 'store_products.store_id', '=', 'stores.id')
			->leftJoin('import_files', 'store_products.file_id', '=', 'import_files.id')
			->select('store_products.*', 'import_files.file_name', 'stores.name as store_name')
			->paginate(150);
		
		return view('productsList')->with([
			'files' => $files,
			'products' => $products,
		]);

	}

	/**
	 * import products from files
	 *
	 * @param Request $request
	 */
	public function importProductsFromFile(Request $request)
	{
		$threadHashesId = $request->hashesId;

		if ($threadHashesId) {
			$pathToArtisan = base_path('artisan');
			define('ARTISAN_BINARY', $pathToArtisan);

			Artisan::call("importFileData", ['--threadHashesId' => $threadHashesId]);
		}
	}
}
