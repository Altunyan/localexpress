<?php
// url parser
if(!function_exists('url_path')) {
    function url_path() {
        $parsed_url = parse_url(url()->current(), PHP_URL_PATH);

        return $parsed_url;
    }
}