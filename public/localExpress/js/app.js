import 'popper.js';
import 'bootstrap';
import './global';
import './modules/tree';

//importing js from modules
import initUploadFiles from './modules/upload_files';

class Main {
    constructor() {
        //activating modules js
        initUploadFiles();
    }
}

new Main();