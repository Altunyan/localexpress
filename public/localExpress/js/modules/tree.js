const tree = {
    value: 1,
    left: {
        value: 2,
        left: {
            value: 3,
            left: {
                value: 4
            },
            right: {
                value: 5
            }
        },
        right: {
            value: 6
        }
    },
    right: {
        value: 7,
        left: {
            value: 8,
            left: {
                value: 10
            },
            right: {
                value: 11
            }
        },
        right: {
            value: 9,
            left: {
                value: 12
            },
            right: {
                value: 13,
                right: {
                    value: 14
                }
            }
        }
    }
};

function objectDeepKeys(obj){
    return Object.keys(obj)
        .filter(key => obj[key] instanceof Object)
        .map(key => objectDeepKeys(obj[key]).map(k => `${key}.${k}`))
        .reduce((x, y) => x.concat(y), Object.keys(obj))
}

const printTree = () => {
    let keys = objectDeepKeys(tree),
        newTree = [];

    keys.forEach((key) => {
        if (key === 'value') {
            newTree.push(tree[key]);
        } else {
            let val = key.split('.value'),
                index = val[0].split('.').length;
            if (val.length > 1) {
                if (!newTree[index]) newTree[index] = [];

                newTree[index].push(eval('tree.' + key))
            }
        }
    });

    console.log(newTree);
};
printTree();

//another way
const printTree2 = (tree) => {
    if(Array.isArray(tree) ){
        let string = '',
            new_tree = [];

        tree.forEach((value) => {
            if(value.left){
                string += value.left.value + " ";
                new_tree.push(value.left);
            }
            if(value.right){
                string += value.right.value + " ";
                new_tree.push(value.right);
            }

        });

        string = string.slice(0, -1);

        console.log(string);

        if(new_tree.length>0){
            printTree2(new_tree)
        }
    }else{
        console.log(tree.value.toString());
        printTree2([tree])
    }

};

printTree2(tree);