const xhrPromise = require('xhr-promise');

class UploadFile {
    constructor() {
        this.uploadFileBtn = document.getElementById('upload-file');
        this.uploadFileBtnSpinner = this.uploadFileBtn.querySelector('.spinner-border');
        this.uploadFileAlert = document.querySelector('.upload-file-alert');
        this.filesInput = document.getElementById('files');
        this.maxFilesSize = 5 * 1024 * 1024;//5MB

        this.uploadFileBtn.addEventListener('click', () => {
            this.toggleUploadBtnIsActive(false);
            this.uploadFiles();
        });
    };

    uploadFiles() {
        let formData = new FormData(),
            files = Array.from(this.filesInput.files),
            store = document.getElementById('select-store').value,
            size = 0;

        files.map(function (file, i) {
            size += file.size;
            formData.append('files[]', file)
        });

        if (size > this.maxFilesSize) {
            this.toggleUploadBtnIsActive();
            this.uploadFileAlert.classList.remove('hidden');
        } else {
            this.uploadFileAlert.classList.add('hidden');

            formData.append('store', store);

            sendXMLHttpRequest('/uploadFiles', formData, (res) => {
                let response = JSON.parse(res.response);
                let status = response.status;

                if (status === 200) {
                    sendXhr('/importProductsFromFile', {hashesId: response.hashesId});

                    window.location.href = response.redirectUrl;
                }
            })
        }
    }

    toggleUploadBtnIsActive(active = true) {
        if (active) {
            this.uploadFileBtn.disabled = false;
            this.uploadFileBtnSpinner.classList.add("hidden");
        } else {
            this.uploadFileBtn.disabled = true;
            this.uploadFileBtnSpinner.classList.remove("hidden");
        }
    }

    getCountries() {
        new xhrPromise().send({
            method: 'POST',
            headers: {'X-CSRF-TOKEN': this.token, 'Content-Type': 'application/json'},
            url: '/getCountries',
        })
            .then((res) => {
                this.countries = res.responseText
            })
    }

    static _init() {
        if (document.querySelector('[data-module="upload-files"]'))
            return new UploadFile();
    }
}

export default UploadFile._init;
