<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned()->index();
			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
			$table->string('real_name')->index();
            $table->string('file_name');
            $table->enum('status', ['new', 'processing', 'done'])->default('new')->index();
            $table->integer('size');
            $table->integer('error_columns_count')->nullable();
            $table->dateTime('import_starting_time')->nullable();
            $table->string('thread_hash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_files');
    }
}
