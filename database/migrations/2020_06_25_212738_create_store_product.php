<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('store_id')->unsigned()->index();
			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('file_id')->unsigned()->index();
			$table->foreign('file_id')->references('id')->on('import_files')->onDelete('cascade')->onUpdate('cascade');
			$table->string('upc')->nullable()->index();
			$table->string('title')->nullable();
			$table->string('price')->nullable();
			$table->tinyInteger('is_error')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_products');
    }
}
