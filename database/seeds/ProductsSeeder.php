<?php

use App\Models\MySQL\MiniatureProduct;
use App\Models\MySQL\MiniatureStoreProduct;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = [
        	'image1.png',
        	'image1.jpg',
			'image2.jpg',
			'image3.jpg',
			md5(time())
		];

		for ($i = 0; $i < 1000; $i++) {
			MiniatureProduct::updateOrCreate(['id' => $i + 1], [
				'id' => $i + 1,
				'image' => $images[rand(0, count($images) - 1)],
				'is_deleted' => rand(0, 1)
			]);
		}

		for ($i = 0; $i < 600; $i++) {
			MiniatureStoreProduct::updateOrCreate(['id' => $i + 1], [
				'id' => $i + 1,
				'product_id' => rand(1, 1000),
				'product_image' => $images[rand(0, count($images) - 1)],
			]);
		}
    }
}
