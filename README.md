# Documentation
- Create .env file in root directory for runing laravel.
- Copy all from .env.example and add to .env.
- Generate app key for laravel.
    ```
       php artisan key:generate
    ```
- Create .env file and copy content from .env.example and paste in .env.
- Add db configs in .env.
- Create tables in db.
    ```
       php artisan migrate
    ```
- Install composer.
    ```
        composer install
    ```   
- Install npm.
    ```
        npm install
    ```
- After installation npm run webpack.
    ```
        npm run build
    ```
- Add products to db for miniature task.
    ```
       php artisan db:seeder --class=ProductsSeeder
    ```
- Miniature task command.

    php artisan generateProductMiniature {size} --watermarked=true/false(optional argument default false) 
                                        --catalogOnly=true(optional argument default true).
                                        
    Examples
                                    
    ```
       php artisan generateProductMiniature 100
    ```
    ```
       php artisan generateProductMiniature 100x200 --watermarked=false --catalogOnly=true
    ```
    
